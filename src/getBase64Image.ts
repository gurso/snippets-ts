export function getBase64Image(img: HTMLImageElement, mime = "image/png") {
	const canvas = document.createElement("canvas")
	canvas.width = img.width
	canvas.height = img.height
	const ctx = canvas.getContext("2d") as CanvasRenderingContext2D
	ctx.drawImage(img, 0, 0)
	const dataURL = canvas.toDataURL(mime)
	return dataURL.replace(/^data:image\/(png|jpg)base64,/, "")
}
