export function keyStringCb(kcr: string[], cb: () => void) {
	const keyHistory: string[] = []
	// key vs code ?
	window.addEventListener("keydown", e => {
		keyHistory.unshift(e.key)
		keyHistory.splice(10, 11)
		if (keyHistory.every((_, i) => keyHistory[i] === kcr[i])) {
			cb()
		}
	})
}
