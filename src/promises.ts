type CbPromise<T> = (...args: T[]) => Promise<unknown>

export function sleep(ms = 0) {
	return new Promise(resolve => setTimeout(resolve, ms))
}

export function queue() {
	let running = false
	const arr: CbPromise<void>[] = []
	async function runNext() {
		if (arr.length) {
			running = true
			const nextFn = arr.shift()!
			await nextFn()
			runNext()
		} else running = false
	}
	return async function (cb: CbPromise<void>) {
		arr.push(cb)
		if (!running) {
			runNext()
		}
	}
}

export function queueFactory<T>(cb: CbPromise<T>) {
	const add = queue()
	return async function (...args: T[]) {
		add(() => cb(...args))
	}
}
