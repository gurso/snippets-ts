export function equal(a: Readonly<unknown>, b: Readonly<unknown>): boolean {
	if (a === b) return true
	if (typeof a !== typeof b) return false
	if (a === null || b === null) return false
	if (Number.isNaN(a) && Number.isNaN(b)) return true
	if (typeof a !== "object" || typeof b !== "object") return false
	if ((a as any).constructor !== (b as any).constructor) return false
	// if ((a as any).prototype !== (b as any).prototype) return false
	if (a instanceof Date) return a.getTime() === (b as Date).getTime()
	if (a instanceof Map || a instanceof Set) return equal(Array.from(a.entries()), Array.from((b as typeof a).entries()))
	// && equal(Object.entries(a),Object.entries(b)) // overkill
	const keys = Object.keys(a) as (keyof typeof a)[]
	if (keys.length !== Object.keys(b).length) return false
	return keys.every(k => equal(a[k], b[k]))
}

export function freeze<T = object>(object: T): Readonly<T> {
	const propNames = Object.getOwnPropertyNames(object)
	for (const name of propNames) {
		const value = object[name as keyof T]
		if (value && typeof value === "object") {
			freeze(value)
		}
	}
	return Object.freeze(object)
}
