export const isSafeNumber: (v: unknown) => v is number = Number.isFinite as (v: unknown) => v is number

export function orderOf(n: number): number {
	const nb = Math.abs(n)
	if (isSafeNumber(nb)) {
		if (nb >= 1) return Math.pow(10, Math.trunc(Math.log10(nb)))
		else if (nb === 0) return 0
		else return Math.pow(10, Math.trunc(Math.log10(nb)) - 1)
	} else {
		throw new Error("orderOf: need safe number")
	}
}

export function fixNumber(n: number, max = 15): number {
	if (/^\d+\.\d+$/.test(n.toString())) {
		const position = n.toString().split(".")[1].length
		return Number(n.toFixed(position > max ? position - 1 : position))
	} else return n
}

function getOrder4Int(n: number) {
	// only used whith finite number
	// let r = 1
	// while (!Number.isInteger(n)) {
	// 	n = fixNumber(n * 10)
	// 	r *= 10
	// }
	// return r
	return Math.pow(10, n.toString().split(".")[1]?.length ?? 0)
}

/**
 * PGCD in french
 * not real gcd because the function try to handle float value
 * @param a
 * @param b
 * @returns greatest common divisor
 */
export function gcd(a: number, b: number): number {
	a = Math.abs(a)
	b = Math.abs(b)
	if (Number.isInteger(a) && Number.isInteger(b)) {
		do {
			const r = a
			a = b
			b = r % a
		} while (b > 0)
		return a
	} else if (Number.isFinite(a) && Number.isFinite(b)) {
		const i = getOrder4Int(a)
		const j = getOrder4Int(b)
		const dec = i > j ? i : j
		const tmp = gcd(a * dec, b * dec)
		return tmp / dec
	} else if (Number.isFinite(a)) return a
	else if (Number.isFinite(b)) return b
	throw new Error("gcd: cannot find gcd between " + a + " & " + b)
}

/**
 * PPCM in french
 * @param a
 * @param b
 * @returns lowest common multiple
 */
export function lcm(a: Readonly<number>, b: Readonly<number>): number {
	if (Number.isInteger(a) && Number.isInteger(b)) {
		return Math.abs((a * b) / gcd(a, b))
	}
	if (Number.isFinite(a) && Number.isFinite(b)) {
		const i = getOrder4Int(a)
		const j = getOrder4Int(b)
		const dec = i > j ? i : j
		const tmp = lcm(a * dec, b * dec)
		return tmp / dec
	}
	throw new Error("lcm: Need 2 safe number")
}

export function cheatLcm(a: Readonly<number>, b: Readonly<number>): number {
	if (Math.max(a, b) > 2) {
		if (b < a) [a, b] = [b, a]
		// do nothing if args already integer
		a = Math.floor(a)
		b = Math.ceil(b)
	}
	return lcm(a, b)
}

export type Coord = { x: number; y: number }

export function barryCenter(from: Readonly<Coord>, to: Readonly<Coord>, fraction: number): [number, number] {
	return [to.x + (from.x - to.x) * fraction, to.y + (from.y - to.y) * fraction]
}

export function diff(a: Coord, b: Coord) {
	return {
		x: b.x - a.x,
		y: b.y - a.y,
	}
}

export function polarToCartesian({
	cx = 0,
	cy = 0,
	radius = 1,
	angle,
}: {
	cx?: number
	cy?: number
	radius?: number
	angle: number
}) {
	return {
		x: cx + radius * Math.cos(angle),
		y: cy + radius * Math.sin(angle),
	}
}

export function pytHypo(adj: number, op: number) {
	return Math.sqrt(adj * adj + op * op)
}

export function pytRev(hypo: number, side: number) {
	return Math.sqrt(hypo * hypo - side * side)
}

export function radians2degrees(radians: number) {
	return radians * (180 / Math.PI)
}

export function degrees2radians(degrees: number) {
	return degrees * (Math.PI / 180)
}

export function getAngle(from: Coord, to: Coord) {
	const d = diff(to, from)
	const hypo = pytHypo(d.x, d.y)
	if (to.y >= from.y) {
		const r = Math.acos(d.x / hypo)
		return Math.PI - r
	} else if (to.x <= from.x) {
		const r = Math.asin(d.y / hypo)
		return r - Math.PI
	} else if (to.x > from.x) {
		const r = Math.acos(d.x / hypo)
		return r - Math.PI
	} else {
		throw new Error("TODO ???")
	}
}
