class A {
	fork() {
		return new (this.constructor as new () => this)()
	}

	clone() {
		const c = this.fork()
		const data = structuredClone(this)
		return Object.assign(c, data)
	}
}
