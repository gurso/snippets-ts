export function strictIncludes<const T, const U>(v: U, arr: T[]) {
	if (arr.includes(v as unknown as T)) return v as U extends T ? U : T
	else throw new Error("value is not in provide list")
}
