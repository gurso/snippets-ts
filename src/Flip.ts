export class Flip {
	protected list = new Map<HTMLElement, DOMRect>()
	protected options: KeyframeAnimationOptions

	constructor(options: number | KeyframeAnimationOptions) {
		if (Number.isFinite(options)) this.options = { duration: options as number }
		else this.options = options as KeyframeAnimationOptions
	}

	add(...els: HTMLElement[]): this {
		for (const el of els) this.list.set(el, el.getBoundingClientRect())
		return this
	}

	play(): this {
		for (const [el, old] of this.list) {
			const bcr = el.getBoundingClientRect()
			const diff = {
				x: old.x - bcr.x,
				y: old.y - bcr.y,
				w: old.width / bcr.width,
				h: old.height / bcr.height,
			}
			el.animate(
				[
					{
						transform: `translate(${diff.x}px, ${diff.y}px) scale(${diff.w}, ${diff.h})`,
					},
					{
						transform: "none",
					},
				],
				this.options
			)
			el.style.transform = `translate(${diff.x}px, ${diff.y}px)`
		}
		return this
	}

	remove(...els: HTMLElement[]): this {
		for (const el of els) this.list.delete(el)
		return this
	}

	clear(): this {
		this.list.clear()
		return this
	}
}
