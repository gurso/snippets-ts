export function ucf<T extends string>(s: T): Capitalize<Lowercase<T>> {
	return (s.charAt(0).toUpperCase() + s.slice(1).toLowerCase()) as Capitalize<Lowercase<T>>
}

// FROM SNAKE
type SnakeToPascal<S extends string> = S extends `${infer T}_${infer U}`
	? `${Capitalize<T>}${Capitalize<SnakeToPascal<U>>}`
	: S
export function snakeToPascal<T extends string>(s: T) {
	const arr = s.split("_")
	return arr.map(ucf).join("") as SnakeToPascal<T>
}

type SnakeToCamel<S extends string> = S extends `${infer T}_${infer U}` ? `${T}${Capitalize<SnakeToCamel<U>>}` : S
export function snakeToCamel<T extends string>(s: T) {
	const pascal = snakeToPascal(s)
	return pascalToCamel(pascal) as SnakeToCamel<T>
}

type SnakeToKebab<S extends string> = S extends `${infer T}_${infer U}` ? `${T}-${SnakeToKebab<U>}` : S
export function snakeToKebab<T extends string>(s: T) {
	return s.replace("_", "-") as SnakeToKebab<T>
}

// FROM KEBAB
type KebabToPascal<S extends string> = S extends `${infer T}-${infer U}`
	? `${Capitalize<T>}${Capitalize<KebabToPascal<U>>}`
	: S
export function kebabToPascal<T extends string>(s: string) {
	const arr = s.split("-")
	return arr.map(ucf).join("") as KebabToPascal<T>
}

type KebabToCamel<S extends string> = S extends `${infer T}-${infer U}` ? `${T}${Capitalize<KebabToCamel<U>>}` : S
export function kebabToCamel<T extends string>(s: T) {
	const pascal = kebabToPascal(s)
	return pascalToCamel(pascal) as KebabToCamel<T>
}

type KebabToSnake<S extends string> = S extends `${infer T}-${infer U}` ? `${T}_${KebabToPascal<U>}` : S
export function kebabToSnake<T extends string>(s: T) {
	return s.replace("-", "_") as KebabToSnake<T>
}

// FROM CAMEL
export function camelToPascal<T extends string>(s: T) {
	return (s.charAt(0).toUpperCase() + s.slice(1)) as Capitalize<T>
}

type CamelToSnake<S extends string> = S extends `${infer T}${infer U}`
	? `${T extends Capitalize<T> ? "_" : ""}${Lowercase<T>}${CamelToSnake<U>}`
	: S
export function camelToSnake<T extends string>(camel: T) {
	return camel
		.split(/(?=[A-Z])/)
		.map(s => s.toLowerCase())
		.join("_") as CamelToSnake<T>
}

type CamelToKebab<S extends string> = S extends `${infer T}${infer U}`
	? `${T extends Capitalize<T> ? "-" : ""}${Lowercase<T>}${CamelToKebab<U>}`
	: S
export function camelToKebab<T extends string>(camel: T) {
	return camel
		.split(/(?=[A-Z])/)
		.map(s => s.toLowerCase())
		.join("_") as CamelToKebab<T>
}

// FROM PASCAL
export function pascalToCamel<T extends string>(s: T) {
	return (s.charAt(0).toLowerCase() + s.slice(1)) as Uncapitalize<T>
}

type PascalToSnake<S extends string> = CamelToSnake<S>
export function pascalToSnake<T extends string>(s: T) {
	return s
		.split(/(?=[A-Z])/)
		.map(s => s.toLowerCase())
		.join("_") as PascalToSnake<T>
}

type PascalToKebab<S extends string> = CamelToKebab<S>
export function pascalToKebab<T extends string>(s: T) {
	return s
		.split(/(?=[A-Z])/)
		.map(s => s.toLowerCase())
		.join("_") as PascalToKebab<T>
}

// could we pass Generic type as arg ?
// type Deep<T, F> = T extends object ? { [K in keyof T as F<K & string>]: T[K] } : T
type DeepSnakeToPascal<T> = T extends object ? { [K in keyof T as SnakeToPascal<K & string>]: T[K] } : T
type DeepSnakeToCamel<T> = T extends object ? { [K in keyof T as SnakeToCamel<K & string>]: T[K] } : T
type DeepSnakeToKebab<T> = T extends object ? { [K in keyof T as SnakeToKebab<K & string>]: T[K] } : T
type DeepKebabToPascal<T> = T extends object ? { [K in keyof T as KebabToPascal<K & string>]: T[K] } : T
type DeepKebabToCamel<T> = T extends object ? { [K in keyof T as KebabToCamel<K & string>]: T[K] } : T
type DeepKebabToSnake<T> = T extends object ? { [K in keyof T as KebabToSnake<K & string>]: T[K] } : T
type DeepCamelToPascal<T> = T extends object ? { [K in keyof T as DeepCamelToPascal<K & string>]: T[K] } : T
type DeepCamelToKebab<T> = T extends object ? { [K in keyof T as DeepCamelToKebab<K & string>]: T[K] } : T
type DeepCamelToSnake<T> = T extends object ? { [K in keyof T as DeepCamelToSnake<K & string>]: T[K] } : T
type DeepPascalToCamel<T> = T extends object ? { [K in keyof T as DeepPascalToCamel<K & string>]: T[K] } : T
type DeepPascalToKebab<T> = T extends object ? { [K in keyof T as DeepPascalToKebab<K & string>]: T[K] } : T
type DeepPascalToSnake<T> = T extends object ? { [K in keyof T as DeepPascalToSnake<K & string>]: T[K] } : T

type Fn =
	| typeof snakeToPascal
	| typeof snakeToCamel
	| typeof snakeToKebab
	| typeof kebabToPascal
	| typeof kebabToCamel
	| typeof kebabToSnake
	| typeof camelToPascal
	| typeof camelToKebab
	| typeof camelToSnake
	| typeof pascalToCamel
	| typeof pascalToKebab
	| typeof pascalToSnake

export function deepKeyApply<T>(o: T, f: typeof snakeToPascal): DeepSnakeToPascal<T>
export function deepKeyApply<T>(o: T, f: typeof snakeToCamel): DeepSnakeToCamel<T>
export function deepKeyApply<T>(o: T, f: typeof snakeToKebab): DeepSnakeToKebab<T>
export function deepKeyApply<T>(o: T, f: typeof kebabToPascal): DeepKebabToPascal<T>
export function deepKeyApply<T>(o: T, f: typeof kebabToCamel): DeepKebabToCamel<T>
export function deepKeyApply<T>(o: T, f: typeof kebabToSnake): DeepKebabToSnake<T>
export function deepKeyApply<T>(o: T, f: typeof camelToPascal): DeepCamelToPascal<T>
export function deepKeyApply<T>(o: T, f: typeof camelToKebab): DeepCamelToKebab<T>
export function deepKeyApply<T>(o: T, f: typeof camelToSnake): DeepCamelToSnake<T>
export function deepKeyApply<T>(o: T, f: typeof pascalToCamel): DeepPascalToCamel<T>
export function deepKeyApply<T>(o: T, f: typeof pascalToKebab): DeepPascalToKebab<T>
export function deepKeyApply<T>(o: T, f: typeof pascalToSnake): DeepPascalToSnake<T>
export function deepKeyApply<T = unknown>(o: T, f: Fn): T | object {
	if (Array.isArray(o)) return o.map(v => deepKeyApply(v, f as never))
	else if (o && typeof o === "object") {
		const r: Record<string, unknown> = {}
		for (const k in o) {
			const newK = f(k)
			r[newK] = deepKeyApply(o[k], f as never)
		}
		return r
	}
	return o
}

// export function factoryDeepKeyApply(fn: Fn) {
// 	return function (o: unknown) {
// 		return deepKeyApply(o, fn)
// 	}
// }

// export function deepSnakeToPascal<T extends Record<string, unknown>>(o: T) {
// 	return _deepKeyApply(o, snakeToPascal) as DeepSnakeToPascal<T>
// }
//
// export function deepSnakeToCamel<T extends Record<string, unknown>>(o: T) {
// 	return _deepKeyApply(o, snakeToCamel) as DeepSnakeToCamel<T>
// }
