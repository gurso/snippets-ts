type Tup<T, L extends number, R extends T[] = []> = R["length"] extends L ? R : Tup<T, L, [...R, T]>

export function isTuple<T, const L extends number>(arr: T[], l: L): arr is Tup<T, L> {
	return arr.length === l
}

export function assertTuple<T, const L extends number>(arr: T[], l: L): asserts arr is Tup<T, L> {
	if (arr.length !== l) throw new Error("Failed asserts tuple")
}

export function toTupple<T, const L extends number>(arr: T[], l: L): Tup<T, L> {
	assertTuple(arr, l)
	return arr
}

