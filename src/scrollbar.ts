export function getScrollbarInfo(el: HTMLElement) {
	const bcr = el.getBoundingClientRect()
	const height = (bcr.height / el.scrollHeight) * bcr.height
	const top = (el.scrollTop / el.scrollHeight) * bcr.height
	const bottom = height + top
	return { bottom, top, height }
}
