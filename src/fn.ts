type Cb = (...args: unknown[]) => void

export function debounce(cb: Cb, ms = 0) {
	let to: ReturnType<typeof setTimeout>

	return function (...args: unknown[]) {
		if (to) clearTimeout(to)
		to = setTimeout(() => cb(...args), ms)
	}
}

export function leadingDebounce(cb: Cb, ms = 0) {
	let to: ReturnType<typeof setTimeout>

	return function (...args: unknown[]) {
		if (!to) {
			to = setTimeout(() => {
				cb(...args)
				clearTimeout(to)
			}, ms)
		}
	}
}

export function throttle(cb: Cb, ms = 0) {
	let ok = true
	return function (...args: unknown[]) {
		if (ok) {
			cb(...args)
			ok = false
			setTimeout(() => {
				ok = true
			}, ms)
		}
	}
}
