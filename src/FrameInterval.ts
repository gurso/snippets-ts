type Options = {
	duration?: number
	// every?: number // step... delay...
	// startAt?: Date
	// endAt?: Date
}

type Fn = (args: { duration: number; progress: number }) => void

export class FrameInterval {
	#status: "started" | "paused" | "stopped" = "stopped"
	#duration: number
	#startedAt?: number
	#progress = 0
	#offset = 0

	constructor({ duration = 0 }: Options) {
		this.#duration = duration
	}

	get status() {
		return this.#status
	}

	get paused() {
		return this.#status === "paused"
	}

	async start(...fns: Fn[]) {
		this.#status = "started"
		this.#progress = this.#offset
		return new Promise<void>(resolve => {
			const step = (ts: number) => {
				if (!this.#startedAt) this.#startedAt = ts
				this.#progress = this.#offset + ts - this.#startedAt
				for (const fn of fns) {
					fn({ progress: this.#progress, duration: this.#duration })
				}
				if (this.#status === "started" && this.#progress < this.#duration) {
					requestAnimationFrame(step)
				} else if (this.paused) {
					this.#startedAt = undefined
					this.#offset = this.#progress
				} else {
					this.#startedAt = undefined
					this.#progress = this.#offset = 0
					this.stop()
					resolve()
				}
			}
			requestAnimationFrame(step)
		})
	}

	pause() {
		this.#status = "paused"
	}

	stop() {
		this.#status = "stopped"
	}
}
