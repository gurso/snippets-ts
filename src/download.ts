export function download(fileName: string, content: string, mimeType: string): void {
	const blob = new Blob([content], { type: mimeType })
	const link = document.createElement("a")
	link.href = window.URL.createObjectURL(blob)
	link.setAttribute("download", fileName)
	link.setAttribute("target", "_blank")
	document.body.appendChild(link)
	link.click()
}
