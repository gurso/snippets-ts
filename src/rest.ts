type HasId = { id: number }

const BASE_URL = "/"

export function rest<T extends HasId>(entity: string) {
	async function fetchAll() {
		const res = await fetch(BASE_URL + entity)
		return res.json()
	}

	async function fetchById(id: number) {
		const res = await fetch(`${BASE_URL}${entity}/${id}`)
		return res.json()
	}

	async function post(entity: Omit<T, "id">) {
		const res = await fetch(`${BASE_URL}${entity}`, {
			method: "POST",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(entity),
		})
		return res.json()
	}

	async function patch(entity: Partial<T> & HasId) {
		const res = await fetch(`${BASE_URL}${entity}/${entity.id}`, {
			method: "PATCH",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(entity),
		})
		return res.json()
	}

	async function update(entity: T) {
		const res = await fetch(`${BASE_URL}${entity}/${entity.id}`, {
			method: "PUT",
			headers: { "Content-Type": "application/json" },
			body: JSON.stringify(entity),
		})
		return res.json()
	}

	async function remove(entity: T) {
		const res = await fetch(`${BASE_URL}${entity}/${entity.id}`, {
			method: "DELETE",
			headers: { "Content-Type": "application/json" },
		})
		return res.json()
	}

	return { fetchAll, fetchById, post, patch, update, remove }
}
